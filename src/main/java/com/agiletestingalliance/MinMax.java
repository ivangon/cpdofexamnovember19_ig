package com.agiletestingalliance;

public class MinMax {

    public int function(int inputA, int inputB) {
        if (inputB > inputA)
            { return inputB; }
        else
            { return inputA; }
    }

}
