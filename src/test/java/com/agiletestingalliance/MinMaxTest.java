package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

	@Test
	public void functionTestA() throws Exception {
		int result = new MinMax().function(10,2);
		assertEquals( "A > B:" ,10, result);
	}

	@Test
	public void functionTestB() throws Exception {
                int result = new MinMax().function(2,10);
                assertEquals( "B > A:" ,10, result);
        }
                	
}
