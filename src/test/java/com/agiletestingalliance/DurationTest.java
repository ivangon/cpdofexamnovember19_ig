package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class DurationTest {

	@Test
	public void testDur() throws Exception {
		String result = new Duration().dur();
		assertTrue(result.contains("DOF is designed"));
	}

}
